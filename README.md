![alt text](<./docs/source/_static/logo_2.png>){width=35%}

# Introduction
The SafeInCave simulator is developed for simulating the mechanics of salt caverns for storage operations. The finite element implementation uses the FEniCS package. Additionally, it includes different constitutive models for salt rock mechanics. 

## Getting started
The user should start by reading the documentation at folder docs/manual. There you will find the installation steps, simple to complex tutorials, and detailed instructions on how to setup your own simulation cases. In addition, check out our video lectures on the SafeInCave simulator:

1) Tensorial operations (theory): https://youtu.be/w5KX3F_rdzU?si=QQLVBq1NcrvOiS32
2) Tensorial operations (exercises): https://youtu.be/JiN6jwp0RPk?si=K1Qhe3lAxJD4LI5w
3) SafeInCave installation: https://youtu.be/J0KQ-nBMwwU?si=VXUVk60yrxavfT1E
4) Stay tuned for upcoming video lectures.

## Current members 
- [Hermínio Tasinafo Honório] (H.TasinafoHonorio@tudelft.nl),  Maintainer, 2023-present
- [Hadi Hajibeygi] (h.hajibeygi@tudelft.nl), Principal Investigator

## Papers and publications
[1] Honório, H.T, Houben, M., Bisdom, K., van der Linden, A., de Borst, K., Sluys, L.J., Hajibeygi, H. A multi-step calibration strategy for reliable parameter determination of salt rock mechanics constitutive models. Int J Rock Mech Min, 2024 (https://doi.org/10.1016/j.ijrmms.2024.105922)

[2] Honório, H.T, Hajibeygi, H. Three-dimensional multi-physics simulation and sensitivity analysis of cyclic hydrogen storage in salt caverns. Int J Hydrogen Energ, 2024 (https://doi.org/10.1016/j.ijhydene.2024.11.081)

[3] Kumar, K.R., Makhmutov, A., Spiers, C.J., Hajibeygi, H. Geomechanical simulation of energy storage in salt formations. Scientific Reports, 2022 (https://doi.org/10.1038/s41598-021-99161-8)

[4] Kumar, K.R., Hajibeygi, H. Influence of pressure solution and evaporate heterogeneity on the geo-mechanical behavior of salt caverns. The Mechanical Behavior of Salt X, 2022 (https://doi.org/10.1201/9781003295808)

## Acknowledgements
We would like to thank Shell Global Solutions International B.V for sponsoring the project SafeInCave, within which this simulator was developed.

