SafeInCave
==========

The **SafeInCave** is a 3D finite element simulator for gas storage in salt caverns.
The simulator is coded in Python and it uses `FEniCS 2019.1 
<https://fenics.readthedocs.io/projects/ufl/en/2019.1.0/manual/examples.html>`_ 
for the finite elements method implementation.





