.. _input-file-assistant:

InputFileAssistant module
=========================


.. automodule:: InputFileAssistant
   :members:
   :undoc-members:
   :show-inheritance:
